﻿using System;
using System.IO;
using TestPrint;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace ScannerTests
{
    [TestClass]
    public class Placard
    {
        private const string TEST_BARCODE_PATH = @"C:\Users\erodriguez1\Desktop\BARCODE_STRING_DATA.csv";

        [TestMethod]
        public void scanAllBarcodes()
        {
            testSetup setup = new testSetup();
            List<string> list = setup.getTestBarcodes(TEST_BARCODE_PATH);
            List<string> unhandled_barcodes = new List<string>();

            Form1 form = new Form1();

            using (System.IO.StreamWriter file =
                new System.IO.StreamWriter(@"C:\Users\erodriguez1\Desktop\UNHANDLED_BARCODES.txt"))
            {
                foreach (string barcode in list) {
                    try { 
                        form.verifyBarcode(barcode);
                    } catch(ArithmeticException except)
                    {
                        unhandled_barcodes.Add(except.Message);
                        file.WriteLine(except.Message);
                    }
                }
            }
            Assert.IsTrue(unhandled_barcodes.Count == 0);
        }

    }
}
